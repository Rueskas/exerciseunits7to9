/* Sergio Ruescas
*  IES SAN VICENTE 2018
*  Development environments 1 DAM 
*  Program that asks the user to enter three marks and the program
*  output the average
*/ 

#include <iostream>
using namespace std;

int main()
{
    double mark, average = 0;
    bool MarkLessFour = false;
    
    for ( int i = 0 ; i < 3 ; i++ )
    {
        cout<<"Enter your mark? ";
        cin>>mark;
        average += mark;
        
        if ( mark < 4 )
        {
            MarkLessFour = true;
        }
    }
    average /= 3 ;
    
    if ( MarkLessFour )
    {
        if ( average <= 3 )
        {
            cout<<"Your average is "<<(int)average;
        }
        
        else
        {
            cout<<"Your average is "<<3;
        }
    }
    
    else
    {
        cout<<"Your average is "<<(int)average;
    }
}
