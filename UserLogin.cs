/* Sergio Ruescas
*  IES SAN VICENTE 2018
*  Development environments 1 DAM 
*  The program ask to the user the username and password if it 
*  is incorrect the program aks three more times
*  username = DAM / PASSWORD = 1234
*/ 

using System;

class Login
 {
     
    static void Main ()
    {
        string username, password;
        int account = 0;
        bool pass = false;
        
        do
        {
            Console.Write( "Enter your Username: " );
            username = Console.ReadLine();
            Console.Write( "Enter your Password: " );
            password = Console.ReadLine();
            if (username != "DAM" || password != "1234")
            {
                Console.WriteLine( "Incorrect, you can pass" );
                account++;
            }
            else 
            {
                pass = true;
            }
        }   while ( account < 3 && pass == false );
        
        if (pass)
        {
            Console.WriteLine( "Correct, you can pass" );
        }
        else
        {
            Console.WriteLine();
        }
    }
}
